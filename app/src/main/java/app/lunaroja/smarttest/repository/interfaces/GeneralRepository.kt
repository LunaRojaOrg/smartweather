package app.lunaroja.smarttest.repository.interfaces

import app.lunaroja.smarttest.network.model.response.ListWeatherResponse
import io.reactivex.Single

interface GeneralRepository {

    fun getListWeather(country: String, counter: Int): Single<ListWeatherResponse>

}