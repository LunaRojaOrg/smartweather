package app.lunaroja.smarttest.repository

import app.lunaroja.smarttest.network.exception.HandleServiceError
import app.lunaroja.smarttest.network.model.response.ListWeatherResponse
import app.lunaroja.smarttest.network.services.ServicesApi
import app.lunaroja.smarttest.repository.interfaces.GeneralRepository
import app.lunaroja.smarttest.utils.Constants.API_KEY
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RetrofitGeneralRepository
@Inject constructor(
    private val apiServices: ServicesApi
) : HandleServiceError(), GeneralRepository {

    override fun getListWeather(country: String, counter: Int): Single<ListWeatherResponse> {
        return apiServices.getListWeather(country,counter,API_KEY).map { serviceResponse ->
            handleResponse(serviceResponse)
            return@map serviceResponse.body()
        }
    }

}