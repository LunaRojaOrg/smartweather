package app.lunaroja.smarttest.network.services

object EndPoint {

    internal const val BASE_URL = "/data/2.5/"

    internal const val GET_LIST_WEATHER = "$BASE_URL weather"

}