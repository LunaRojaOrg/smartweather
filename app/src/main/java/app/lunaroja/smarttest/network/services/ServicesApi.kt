package app.lunaroja.smarttest.network.services

import app.lunaroja.smarttest.network.model.response.ListWeatherResponse
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface ServicesApi {

    @GET(EndPoint.GET_LIST_WEATHER)
    fun getListWeather(
        @Query("q") country: String,
        @Query("cnt") counter: Int,
        @Query("appid") apiKey: String
    ): Single<Response<ListWeatherResponse>>
}