package app.lunaroja.smarttest.network.interceptor

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import java.io.IOException
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BearerAuthInterceptor
@Inject constructor() : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        return chain.proceed(createRequest(request))
    }

    private fun createRequest(request: Request): Request {
        return request.newBuilder()
            .build()
    }
}