package app.lunaroja.smarttest.ui.dialogs

import android.app.Dialog
import android.content.IntentSender
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import app.lunaroja.smarttest.databinding.DialogAlertBinding

class AlertDialog(activity: FragmentActivity) : Dialog(activity) {

    private lateinit var binding: DialogAlertBinding

    private var alertTitle: String? = ""
    private var alertMessage: String? = ""
    private var btnMessage: String? = null
    private var actionDismiss: (() -> Unit)? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setCancelable(false)
        binding = DialogAlertBinding.inflate(layoutInflater)
        setContentView(binding.root)
        applyBinding()
        hideViews()
    }

    private fun hideViews() {
        if (alertTitle.isNullOrEmpty()) {
            binding.tvTitle.visibility = View.GONE
        }
    }

    private fun applyBinding() {
        binding.tvTitle.text = alertTitle
        binding.tvMessage.text = alertMessage
        binding.btnAccept.setOnClickListener {
            dismiss()
        }
        btnMessage?.let {
            binding.btnAccept.text = it
        }
    }

    override fun dismiss() {
        super.dismiss()
        actionDismiss?.invoke()
    }

    override fun onStart() {
        super.onStart()
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.MATCH_PARENT
        window?.apply {
            setLayout(width, height)
            setBackgroundDrawableResource(android.R.color.transparent)
        }
    }

    class Builder(activity: FragmentActivity) {
        private val dialog: AlertDialog = AlertDialog(activity)

        fun withTitle(title: String?): Builder {
            this.dialog.alertTitle = title
            return this
        }

        fun withMessage(message: String?): Builder {
            this.dialog.alertMessage = message
            return this
        }

        fun withButtonMessage(message: String): Builder {
            this.dialog.btnMessage = message
            return this
        }

        fun withDismissCallback(actionDismiss: () -> Unit): Builder {
            this.dialog.actionDismiss = actionDismiss
            return this
        }

        fun show() {
            dialog.show()
        }
    }
}