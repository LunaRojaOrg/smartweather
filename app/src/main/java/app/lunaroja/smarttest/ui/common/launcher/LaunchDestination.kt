package app.lunaroja.smarttest.ui.common.launcher

enum class LaunchDestination {
    ONBOARDING, AUTHENTICATION
}