package app.lunaroja.smarttest.ui.common

import android.os.CountDownTimer
import androidx.lifecycle.MutableLiveData
import app.lunaroja.smarttest.base.BaseViewModel
import app.lunaroja.smarttest.repository.interfaces.GeneralRepository
import app.lunaroja.smarttest.ui.common.launcher.LaunchDestination
import app.lunaroja.smarttest.utils.Event
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class PrincipalViewModel
@Inject constructor(
    private val generalRepository: GeneralRepository,
) : BaseViewModel() {

    private val disposable = CompositeDisposable()

    private val _launchDestination = MutableLiveData<Event<LaunchDestination>>()
    val launchDestination get() = _launchDestination

    init {
        navigateToOnboarding()
    }

    private fun getWeather(country: String, counter: Int) {
        val task = generalRepository.getListWeather(country, counter)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

        val subscriber = task.subscribe({
            //consultProducts()
        }, {
            //consultProducts()
        })

        disposable.add(subscriber)
    }

    private fun navigateToOnboarding() {
        val timer = object: CountDownTimer(5000, 1000) {
            override fun onTick(millisUntilFinished: Long) {

            }
            override fun onFinish() {
                _launchDestination.value = Event(LaunchDestination.ONBOARDING)
            }
        }
        timer.start()
    }

    override fun onCleared() {
        disposable.clear()
        super.onCleared()
    }
}