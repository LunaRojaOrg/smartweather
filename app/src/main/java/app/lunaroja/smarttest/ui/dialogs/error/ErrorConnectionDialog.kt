package app.lunaroja.smarttest.ui.dialogs.error

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import app.lunaroja.smarttest.R
import app.lunaroja.smarttest.databinding.DialogErrorConnectionBinding

class ErrorConnectionDialog : DialogFragment() {

    private lateinit var binding: DialogErrorConnectionBinding

    companion object {
        internal const val TAG = "TAG_ERROR_CONNECTION_DIALOG"

        fun newInstance(): ErrorConnectionDialog {
            return ErrorConnectionDialog()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.AppTheme_FullScreenDialog)
        isCancelable = false
    }

    override fun onCreateView(inflater: LayoutInflater, group: ViewGroup?, bundle: Bundle?): View {
        binding = DialogErrorConnectionBinding.inflate(layoutInflater, group, false)
        binding.btnClose.setOnClickListener {
            dismiss()
        }
        return binding.root
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.apply {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            setLayout(width, height)
        }
    }

    override fun dismiss() {
        super.dismissAllowingStateLoss()
    }

    override fun show(manager: FragmentManager, tag: String?) {
        val fragmentTransaction = manager.beginTransaction()
        fragmentTransaction.add(this, tag)
        fragmentTransaction.commitAllowingStateLoss()
    }
}