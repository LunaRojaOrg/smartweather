package app.lunaroja.smarttest.ui.common.home

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import app.lunaroja.smarttest.R

class HomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}