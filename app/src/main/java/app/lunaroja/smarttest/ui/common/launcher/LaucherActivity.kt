package app.lunaroja.smarttest.ui.common.launcher

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import app.lunaroja.smarttest.R
import app.lunaroja.smarttest.SmartWeatherApplication
import app.lunaroja.smarttest.databinding.ActivityLauncherBinding
import app.lunaroja.smarttest.di.component.DaggerLauncherComponent
import app.lunaroja.smarttest.factory.LauncherViewModelFactory
import app.lunaroja.smarttest.repository.interfaces.GeneralRepository
import app.lunaroja.smarttest.ui.common.PrincipalViewModel
import app.lunaroja.smarttest.ui.common.home.HomeActivity
import app.lunaroja.smarttest.utils.EventObserver
import javax.inject.Inject

class LauncherActivity : AppCompatActivity() {

    @Inject
    lateinit var generalRepository: GeneralRepository

    lateinit var binding: ActivityLauncherBinding

    private val launcherViewModel by viewModels<PrincipalViewModel> {
        LauncherViewModelFactory(generalRepository, this, intent.extras)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initBinding()
        initInjection()
        launcherViewModel.launchDestination.observe(this, EventObserver { destination ->
            binding.animationLauncher.visibility = GONE
            binding.rootLauncher.visibility = VISIBLE
            when (destination) {
                LaunchDestination.ONBOARDING -> {
                    navigateToActivity(HomeActivity::class.java)
                }
                LaunchDestination.AUTHENTICATION -> {
                    //navigateToActivity(AuthenticationActivity::class.java)
                }
            }
        })
    }

    private fun initBinding() {
        binding = ActivityLauncherBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.rootLauncher.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
    }

    private fun initInjection() {
        val component = DaggerLauncherComponent.builder()
            .applicationComponent(SmartWeatherApplication.applicationComponent)
            .build()
        component.inject(this)
    }

    private fun <T> navigateToActivity(clazz: Class<T>) {
        val intent = Intent(this, clazz)
        startActivity(intent)
        overridePendingTransition(0, R.anim.fade_out)
        finish()
    }
}