package app.lunaroja.smarttest

import android.app.Application
import app.lunaroja.smarttest.di.*
import app.lunaroja.smarttest.di.component.ApplicationComponent
import app.lunaroja.smarttest.di.component.DaggerApplicationComponent
import app.lunaroja.smarttest.di.module.ApplicationModule
import app.lunaroja.smarttest.di.module.DataSourceModule
import app.lunaroja.smarttest.di.module.NetworkModule

class SmartWeatherApplication : Application() {

    companion object {
        var applicationComponent: ApplicationComponent? = null
            private set

        init {

        }
    }

    override fun onCreate() {
        super.onCreate()
        configureDagger()
    }

    private fun configureDagger() {
        applicationComponent = DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(applicationContext))
            .dataSourceModule(DataSourceModule())
            .networkModule(
                NetworkModule(
                    getBaseUrl()
                )
            )
            .build()
    }

    private fun getBaseUrl(): String {
        return applicationContext.getString(R.string.base_url)
    }
}