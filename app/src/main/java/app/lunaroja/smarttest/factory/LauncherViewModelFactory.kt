package app.lunaroja.smarttest.factory

import android.os.Bundle
import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.savedstate.SavedStateRegistryOwner
import app.lunaroja.smarttest.repository.interfaces.GeneralRepository
import app.lunaroja.smarttest.ui.common.PrincipalViewModel

@Suppress("UNCHECKED_CAST")
class LauncherViewModelFactory(
    private val generalRepository: GeneralRepository,
    owner: SavedStateRegistryOwner,
    defaultArgs: Bundle?
) :
    AbstractSavedStateViewModelFactory(owner, defaultArgs) {

    override fun <T : ViewModel?> create(key: String, clazz: Class<T>, state: SavedStateHandle): T {
        val viewModel: ViewModel = if (clazz == PrincipalViewModel::class.java) {
            PrincipalViewModel(generalRepository)
        } else {
            throw RuntimeException("Unsupported view model class: $clazz")
        }
        return viewModel as T
    }
}