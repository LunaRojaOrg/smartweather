package app.lunaroja.smarttest.di.component

import app.lunaroja.smarttest.di.scope.FragmentScope
import app.lunaroja.smarttest.ui.common.launcher.LauncherActivity
import dagger.Component

@FragmentScope
@Component(dependencies = [ApplicationComponent::class])
interface LauncherComponent {

    fun inject(target: LauncherActivity)
}