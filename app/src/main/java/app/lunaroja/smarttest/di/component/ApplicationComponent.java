package app.lunaroja.smarttest.di.component;

import javax.inject.Singleton;

import app.lunaroja.smarttest.di.module.ApplicationModule;
import app.lunaroja.smarttest.di.module.DataSourceModule;
import app.lunaroja.smarttest.di.module.NetworkModule;
import app.lunaroja.smarttest.repository.interfaces.GeneralRepository;
import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class, DataSourceModule.class, NetworkModule.class})
public interface ApplicationComponent {

    GeneralRepository generalRepository();

}