package app.lunaroja.smarttest.di.scope

object Scope {
    const val BASIC_AUTH = "BASIC_AUTH"
    const val BEARER_AUTH = "BEARER_AUTH"
}