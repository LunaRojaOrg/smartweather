package app.lunaroja.smarttest.di.module

import app.lunaroja.smarttest.di.scope.Scope
import app.lunaroja.smarttest.network.interceptor.BearerAuthInterceptor
import app.lunaroja.smarttest.network.services.ServicesApi
import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

@Module
class NetworkModule(private val baseUrl: String) {

    companion object {
        private const val REQUEST_TIMEOUT = 90L
    }

    @Singleton
    @Provides
    @Named(Scope.BEARER_AUTH)
    fun provideBearerInterceptor(interceptor: BearerAuthInterceptor): Interceptor {
        return interceptor
    }

    @Singleton
    @Provides
    fun provideSmartWeatherAPI(@Named(Scope.BEARER_AUTH) interceptor: Interceptor): ServicesApi {
        val client = createHttpClient()
            .addInterceptor(interceptor)
            .build()

        return createRetrofit()
            .client(client)
            .build()
            .create(ServicesApi::class.java)
    }

    private fun createRetrofit(): Retrofit.Builder {
        val builder = GsonBuilder()
            .disableHtmlEscaping()
            .setPrettyPrinting()
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .create()

        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create(builder))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
    }

    private fun createHttpClient(): OkHttpClient.Builder {
        val httpLogInterceptor =
            HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)

        return OkHttpClient.Builder()
            .connectTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
            .writeTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
            .addInterceptor(httpLogInterceptor)
    }
}