package app.lunaroja.smarttest.di.module

import app.lunaroja.smarttest.repository.RetrofitGeneralRepository
import app.lunaroja.smarttest.repository.interfaces.GeneralRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DataSourceModule {

    @Singleton
    @Provides
    fun provideGeneralRepository(retrofitGeneralRepository: RetrofitGeneralRepository): GeneralRepository {
        return retrofitGeneralRepository;
    }

}